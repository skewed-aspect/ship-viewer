// ---------------------------------------------------------------------------------------------------------------------
// Model Viewer Component
// ---------------------------------------------------------------------------------------------------------------------

// Engine
import { RenderEngine } from '../lib/engine.ts';

// Styles
import modelViewerStyleText from '../styles/model-viewer.scss?inline';

// ---------------------------------------------------------------------------------------------------------------------

const modelViewerStyle = document.createElement('style');
modelViewerStyle.appendChild(document.createTextNode(modelViewerStyleText));

// ---------------------------------------------------------------------------------------------------------------------

export class ModelViewer extends HTMLElement
{
    #initialized = false;
    #engine ?: RenderEngine;
    #elem = document.createElement('canvas');
    #loadingElem = document.createElement('div');
    #axisOrder : [ string, string, string ] = [ 'x', 'y', 'z' ];
    #modelDims = {
        loa: 0,
        beam: 0,
        draft: 0
    };
    #skyboxEnabled = true;

    constructor()
    {
        super()
        this.attachShadow({ mode: 'open' });

        // Prepare our element
        this.#elem.id = 'model-viewer';
        this.#elem.setAttribute('touch-action', 'none');

        // Prevent scrolling on page
        this.#elem.addEventListener('wheel', (evt) => evt.preventDefault());

        // Set up Loading Screen
        const innerElem = document.createElement('div');
        innerElem.className = 'inner';

        const loadingText = document.createElement('div');
        loadingText.id = 'loading-text';
        loadingText.className = 'container';
        loadingText.innerText = "System Initializing..."

        this.#loadingElem.id = 'loading';
        this.#loadingElem.appendChild(innerElem);
        this.#loadingElem.appendChild(loadingText);

        // Add our element to the root
        if(this.shadowRoot)
        {
            this.shadowRoot.appendChild(this.#loadingElem);
            this.shadowRoot.appendChild(this.#elem);
        }
    }

    buildUI() : void
    {
        const fsBtn = document.createElement('button');
        fsBtn.id = 'fs-btn';
        fsBtn.innerText = "Fullscreen";
        fsBtn.onclick = () =>
        {
            if(this.#engine)
            {
                this.#engine.fullscreen();
            }
        }

        const sbBtn = document.createElement('button');
        sbBtn.id = 'sb-btn';
        sbBtn.innerText = this.#skyboxEnabled ? 'Disable Skybox' : 'Enable Skybox';
        sbBtn.onclick = () =>
        {
            if(this.#engine)
            {
                // Toggle skybox state
                this.#skyboxEnabled = !this.#skyboxEnabled;

                // Toggle the skybox appropriately.
                if(this.#skyboxEnabled)
                {
                    sbBtn.innerText = 'Disable Skybox';
                    this.#engine?.showSkybox();
                }
                else
                {
                    sbBtn.innerText = 'Enable Skybox';
                    this.#engine?.hideSkybox();
                }
            }
        }

        const modelDimsElem = document.createElement('div');
        modelDimsElem.id = 'model-dimensions';
        modelDimsElem.className = 'container';
        modelDimsElem.innerText = [
            `LOA: ${ this.#modelDims.loa }m`,
            `Beam: ${ this.#modelDims.beam }m`,
            `Draft: ${ this.#modelDims.draft }m`
        ].join(', ');

        if(this.shadowRoot)
        {
            this.shadowRoot.appendChild(fsBtn);
            this.shadowRoot.appendChild(sbBtn);
            this.shadowRoot.appendChild(modelDimsElem);
        }
    }

    initialize() : void
    {
        console.log('Ship Viewer initializing...');

        // Pull the stating value for the skybox
        const noSkybox = this.getAttribute('skybox') === 'false';
        this.#skyboxEnabled = !noSkybox;

        // Pull value for axis order
        const axisOrder = (this.getAttribute('axis-order') ?? 'x,y,z')
            .split(',')
            .map((item) => item.trim().toLowerCase()
            );
        this.#axisOrder = [
            axisOrder[0] ?? 'x',
            axisOrder[1] ?? 'y',
            axisOrder[2] ?? 'z',
        ];

        // Add styles
        if(this.shadowRoot)
        {
            this.shadowRoot.appendChild(modelViewerStyle.cloneNode(true));
        }

        const lightIntensity = parseFloat(this.getAttribute('light') ?? '0.75') ?? 0.75;

        // Build engine
        this.#engine = new RenderEngine(this.#elem, lightIntensity);
        this.#engine.start();

        // Toggle the skybox appropriately.
        if(this.#skyboxEnabled)
        {
            this.#engine?.showSkybox();
        }
        else
        {
            this.#engine?.hideSkybox();
        }

        // Grab the model
        const model = this.getAttribute('model');
        const scale = parseFloat(this.getAttribute('scale') ?? '1.0');

        if(model)
        {
            // Pull the camera settings
            const alpha = parseFloat(this.getAttribute('alpha') ?? '0');
            const beta = parseFloat(this.getAttribute('beta') ?? '0');
            const radius = parseFloat(this.getAttribute('radius') ?? '0');

            this.#engine?.loadModel(model, scale, alpha, beta, radius)
                .then((bounds) =>
                {
                    if(!this.#skyboxEnabled)
                    {
                        // Enable edge rendering
                        this.#engine?.enableEdgeRendering();
                    }

                    // Work out model dimensions
                    const modelBBox = bounds.extendSize.scale(2)
                    const dims = {
                        y: parseInt(modelBBox.y.toFixed(0)),
                        x: parseInt(modelBBox.x.toFixed(0)),
                        z: parseInt(modelBBox.z.toFixed(0))
                    }

                    this.#modelDims.loa = dims[this.#axisOrder[0]] ?? dims.x;
                    this.#modelDims.beam = dims[this.#axisOrder[1]] ?? dims.y;
                    this.#modelDims.draft = dims[this.#axisOrder[2]] ?? dims.z;

                    // Give the engine a chance to render it. This is a hack, but I'm too lazy to figure out the right
                    // way to do this, and the loading screen's pretty.
                    return new Promise((resolve) => setTimeout(resolve, 2000));
                })
                .then(() =>
                {
                    this.#loadingElem.className = 'hidden';
                    this.buildUI();
                });
        }
    }

    connectedCallback()
    {
        if(!this.#initialized)
        {
            this.initialize();
            this.#initialized = true;
        }
    }
}

// ---------------------------------------------------------------------------------------------------------------------
