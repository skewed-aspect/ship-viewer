// ---------------------------------------------------------------------------------------------------------------------
// Babylon Engine
// ---------------------------------------------------------------------------------------------------------------------

import {
    ArcRotateCamera,
    BoundingBox,
    BoundingInfo,
    Color3,
    Color4,
    CreateBox,
    CubeTexture,
    DefaultRenderingPipeline,
    Engine,
    GlowLayer,
    HemisphericLight,
    Mesh,
    PointLight,
    Scene,
    SceneLoader,
    StandardMaterial,
    Texture,
    Vector3
} from '@babylonjs/core';

import "@babylonjs/loaders";

// Skybox Textures (imported so it's baked in)
import SkyboxPX from "../assets/textures/purplenebula_2048_right1.webp";
import SkyboxPY from "../assets/textures/purplenebula_2048_top3.webp";
import SkyboxPZ from "../assets/textures/purplenebula_2048_front5.webp";
import SkyboxNX from "../assets/textures/purplenebula_2048_left2.webp";
import SkyboxNY from "../assets/textures/purplenebula_2048_bottom4.webp";
import SkyboxNZ from "../assets/textures/purplenebula_2048_back6.webp";

// ---------------------------------------------------------------------------------------------------------------------

export class RenderEngine
{
    #canvas : HTMLCanvasElement;
    #lightIntensity : number;
    #engine : Engine;
    #scene : Scene;
    #camera : ArcRotateCamera;
    #skybox ?: Mesh;
    #light ?: HemisphericLight;
    #model ?: Mesh;

    constructor(canvas : HTMLCanvasElement, lightIntensity = 0.75)
    {
        this.#canvas = canvas;
        this.#lightIntensity = lightIntensity;
        this.#engine = new Engine(this.#canvas);
        this.#scene = new Scene(this.#engine);
        this.#scene.clearColor = Color4.FromColor3(Color3.Black());

        this.#camera = new ArcRotateCamera('model_cam', 0, 0, 10, Vector3.Zero(), this.#scene, true);

        this.#camera.setTarget(Vector3.Zero());
        this.#camera.attachControl(this.#canvas, true);
        this.#camera.maxZ = 1000000000;

        // Build the Scene
        this.buildScene();
    }

    private buildSkybox() : void
    {
        // So, this is weird. We're importing these as inline images, so we don't have to serve these resources, so we
        // have to jump through some weird hoops to get whatever random url vite uses for this.
        const skyboxFiles = [
            SkyboxPX,
            SkyboxPY,
            SkyboxPZ,
            SkyboxNX,
            SkyboxNY,
            SkyboxNZ,
        ];

        this.#skybox = CreateBox('skyBox', { size: 100000.0 }, this.#scene);
        const skyboxMaterial = new StandardMaterial('skyBox', this.#scene);
        skyboxMaterial.backFaceCulling = false;
        skyboxMaterial.diffuseColor = new Color3(0, 0, 0);
        skyboxMaterial.specularColor = new Color3(0, 0, 0);
        skyboxMaterial.reflectionTexture = new CubeTexture(
            '',
            this.#scene,
            [],
            true,
            skyboxFiles
        );
        skyboxMaterial.reflectionTexture.coordinatesMode = Texture.SKYBOX_MODE;

        // This keeps the mesh a fixed distance from the camera.
        this.#skybox.infiniteDistance = true;

        // Set the material
        this.#skybox.material = skyboxMaterial;
    }

    private buildScene() : void
    {
        this.buildSkybox();

        const pipeline = new DefaultRenderingPipeline("defaultPipeline", true, this.#scene, [this.#camera]);
        pipeline.samples = 32;
        pipeline.sharpenEnabled = true;

        // Create a basic light, aiming 0,1,0 - meaning, to the sky.
        this.#light = new HemisphericLight('ambientLight', new Vector3(0, 1, 0), this.#scene);
        this.#light.intensity = this.#lightIntensity;
        this.#light.specular = Color3.Black();
        this.#light.groundColor = Color3.Purple();

        const pointLight = new PointLight('pointLight', new Vector3(20, 20, 100), this.#scene);
        pointLight.intensity = 0.8;
        pointLight.specular = Color3.Black();
        this.#scene.onBeforeRenderObservable.add(() =>
        {
            pointLight.position = this.#camera.position;
        });

        // Add a Glow layer
        const gl = new GlowLayer('glow', this.#scene);
        gl.intensity = 1.0;
    }

    public enableEdgeRendering() : void
    {
        if(this.#model)
        {
            const boundMax = this.#model.getBoundingInfo().boundingBox.maximumWorld;
            const boundMin = this.#model.getBoundingInfo().boundingBox.minimumWorld;
            const edgeWidth = boundMax.subtract(boundMin).length() * .05;

            // Set edge rendering
            this.#model.getChildMeshes().forEach((mesh) =>
            {
                mesh.enableEdgesRendering();
                mesh.edgesWidth = edgeWidth;
                mesh.edgesColor = Color4.FromColor3(Color3.Black());
            });
        }
    }

    public disableEdgeRendering() : void
    {
        if(this.#model)
        {
            // Set edge rendering
            this.#model.getChildMeshes().forEach((mesh) =>
            {
                mesh.disableEdgesRendering();
            });
        }
    }

    public async loadModel(url : string, scale = 1, alpha = 0, beta = 0, radius = 0) : Promise<BoundingBox>
    {
        const pathArray = url.split('/');
        const file = pathArray.pop();
        const filePath = pathArray.join('/') + '/';

        return new Promise((resolve, reject) => {
            SceneLoader.ImportMesh('', filePath, file, this.#scene, (newMeshes) =>
            {
                // Build new mesh parent
                const parentMesh = new Mesh('model', this.#scene);

                // Build bounding information
                let boundMin = new Vector3(Number.MAX_VALUE, Number.MAX_VALUE, Number.MAX_VALUE);
                let boundMax = new Vector3(Number.MIN_VALUE, Number.MIN_VALUE, Number.MIN_VALUE);
                newMeshes.forEach((mesh) =>
                {
                    const meshBoundMin = mesh.getBoundingInfo().boundingBox.minimumWorld;
                    const meshBoundMax = mesh.getBoundingInfo().boundingBox.maximumWorld;

                    boundMin = Vector3.Minimize(boundMin, meshBoundMin);
                    boundMax = Vector3.Maximize(boundMax, meshBoundMax);

                    mesh.setParent(parentMesh);
                });

                // Set Bounding Information
                parentMesh.setBoundingInfo(new BoundingInfo(boundMin, boundMax));
                parentMesh.computeWorldMatrix(true);

                // Scale model
                parentMesh.scaling.set(scale, scale, scale);

                // Check to see if we have any materials set
                const hasMaterial = parentMesh.getChildMeshes().some((mesh) =>
                {
                    return !!mesh.material;
                });

                // If we don't have any materials, set a default one
                if(!hasMaterial)
                {
                    const material = new StandardMaterial('default', this.#scene);
                    material.diffuseColor = Color3.FromHexString('#999999');
                    material.specularColor = Color3.Black();
                    material.emissiveColor = Color3.Black();
                    material.ambientColor = Color3.Black();
                    material.alpha = 1;
                    material.backFaceCulling = false;
                    material.freeze();

                    // Set all meshes
                    parentMesh.getChildMeshes().forEach((mesh) => mesh.material = material);
                }

                // Set the loaded model
                this.#model = parentMesh;

                // Point the camera at the model by default.
                this.#camera.setTarget(parentMesh.getBoundingInfo().boundingBox.center, true, true);

                // Convert degrees to radians
                alpha *= Math.PI / 180;
                beta *= Math.PI / 180;

                if(radius === 0)
                {
                    radius = parentMesh.getBoundingInfo().boundingSphere.radiusWorld * 1.5;
                }

                // Set the configured camera angles
                this.#camera.alpha = alpha;
                this.#camera.beta = beta;
                this.#camera.radius = radius;

                // Resolve the loading promise
                resolve(parentMesh.getBoundingInfo().boundingBox);
            }, null, reject);
        });
    }

    public setCamera(alpha = 0, beta = 0, radius = 10) : void
    {
        // Convert degrees to radians
        alpha *= Math.PI / 180;
        beta *= Math.PI / 180;

        // Set the configured camera angles
        this.#camera.alpha = alpha;
        this.#camera.beta = beta;
        this.#camera.radius = radius;
    }

    public showSkybox() : void
    {
        if(this.#skybox)
        {
            this.#skybox.visibility = 1;
        }

        if(this.#light)
        {
            this.#light.groundColor = Color3.Purple();
        }

        if(this.#model)
        {
            this.disableEdgeRendering();
        }
    }

    public hideSkybox() : void
    {
        if(this.#skybox)
        {
            this.#skybox.visibility = 0;
        }

        if(this.#light)
        {
            this.#light.groundColor = Color3.White();
        }

        if(this.#model)
        {
            this.enableEdgeRendering();
        }
    }

    public fullscreen() : void
    {
        this.#engine?.switchFullscreen(false);
    }

    public start() : void
    {
        // The canvas/window resize event handler.
        window.addEventListener('resize', () =>
        {
            this.#engine.resize();
        });

        // Set up the render loop
        this.#engine.runRenderLoop(() =>
        {
            this.#scene.render();
        });
    }
}

// ---------------------------------------------------------------------------------------------------------------------
