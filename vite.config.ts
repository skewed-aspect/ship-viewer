//----------------------------------------------------------------------------------------------------------------------
// Vite Config
//----------------------------------------------------------------------------------------------------------------------

import path from 'node:path';
import { defineConfig } from 'vite';

//----------------------------------------------------------------------------------------------------------------------

export default defineConfig({
    root: 'src/demo',
    publicDir: 'assets',
    assetsInclude: [
        '**/*.glb'
    ],
    server: {
        port: 4200,
    },
    build: {
        lib: {
            entry: path.resolve(__dirname, 'src/components.ts'),
            name: 'ModelViewer',
            fileName: (format) => `model-viewer.${format}.js`
        },
        outDir: '../../dist/',
        emptyOutDir: true,
        cssCodeSplit: true,
    }
});

//----------------------------------------------------------------------------------------------------------------------
