# Model Viewer

This is a simple [Web Component][] to render a 3D model with some basic controls. The idea is that this can be a 
little widget on a web page (like a wiki) that displays a nice default view of the model, but can be expanded to full 
screen.

## Usage

### Installation

To install, you need to include the `model-viewer.es.js` or `model-viewer.umd.js` file in your page. For the moment, 
you need to pull this project down and run `npm run build` to generate the files.

### Component API

To use the component, you need to specify a model:

```html
<model-viewer model="/uploads/models/models/ares.stl"></model-viewer>
```

That is the most basic form of using the element. However, it supports several other properties.

#### Setting model scale

* Property: `scale` - The scale multiplier. `1.0` is 100% scale, `0.5` is 50%, `2.5` is 250%, etc.

```html
<model-viewer model="/uploads/models/models/ares.stl" scale="0.125"></model-viewer>
```

This sets the scale of the model, as a floating point scale. This helps you have less giant radiuses on large models.

#### Setting camera position

* Properties: `alpha`, `beta`, `radius` - The `alpha` (x-axis) and `beta`(y-axis) angles for the camera, plus the 
  radius (distance from the centerpoint of the model), as numbers. (_Note: Radius can be left out to auto calculate 
  it._)

```html
<model-viewer model="/uploads/models/models/ares.stl" alpha="300" beta="75" radius="25"></model-viewer>
```

This sets the camera position. The camera always looks at the model, but the rotational angles and the radius can be 
set so that the initial view can be controlled. Leaving the `radius` property off will cause the radius to be 
calculated based on the size of the model.

#### Disabling the skybox

* Property: `skybox` - If this property is set to `false`, the skybox will be disabled.

This turns off the skybox, if present. (This property is only the initial state.)

[Web Component]: https://developer.mozilla.org/en-US/docs/Web/Web_Components
